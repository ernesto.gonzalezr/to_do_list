package com.softtek.academy.javaweb.controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.beans.ListaBean;
import com.softtek.academy.javaweb.dao.To_Do_List_DAO;


public class GetLista extends HttpServlet{
	public GetLista() {
        super();}
        
	
	
	
	
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
        	String var=request.getParameter("is_done");
            Boolean is_done=Boolean.parseBoolean(var);
            
            if(is_done==false) {
            request.setAttribute("list", To_Do_List_DAO.getLista(false));
            request.getRequestDispatcher("/JSP/to_do_list.jsp").forward(request, response);}
            else {
            request.setAttribute("list", To_Do_List_DAO.getLista(true));
            request.getRequestDispatcher("/JSP/done_list.jsp").forward(request, response);}
        }
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
        	
        	doGet(request,response);
        }
        
        
        
}
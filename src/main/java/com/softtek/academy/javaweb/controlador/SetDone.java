package com.softtek.academy.javaweb.controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.beans.ListaBean;
import com.softtek.academy.javaweb.dao.To_Do_List_DAO;


public class SetDone extends HttpServlet{
	public SetDone() {
        super();}
        
	
	
	
	
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
        
        	String cad=request.getParameter("id");
            int id=Integer.parseInt(cad);
        	To_Do_List_DAO.SetDone(id);
            request.setAttribute("list", To_Do_List_DAO.getListaDone());
            request.getRequestDispatcher("/JSP/done_list.jsp").forward(request, response);

       }
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
        	
        	doGet(request,response);
        }
}
package com.softtek.academy.javaweb.dao;

 

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

 

import com.softtek.academy.javaweb.beans.ListaBean;
import com.softtek.academy.javaweb.conexion.Conexion;

 

public class To_Do_List_DAO {
	
	public static void SetDone(int id) {
		Conexion co=new Conexion();
		try {
			Connection con = co.getConnection();
	        String query = "Update to_do_list set is_done=1 where id= ?";
	        PreparedStatement preparedStmt = con.prepareStatement(query);
	        preparedStmt.setInt   (1, id);
	        preparedStmt.executeUpdate();
	     }catch(Exception e) {
	           System.out.println(e);
	     }
	}
	   
	public static void InsertTask(String texto) {
		Conexion co=new Conexion();
		try {
			Connection con = co.getConnection();
	        System.out.println(texto+"este eslllllll");
	        String query = "insert into to_do_list(list,is_done) values('"+texto+"',0);";
	        PreparedStatement preparedStmt = con.prepareStatement(query);
	        preparedStmt.executeUpdate();
	    }catch(Exception e) {
	           System.out.println(e);
	    }
	}
	
	public static List<ListaBean> getLista(Boolean is_done) {
	    List<ListaBean> texto = new ArrayList<ListaBean>();
	    Conexion co=new Conexion();
	    try {
	    	Connection con = co.getConnection();
	    	PreparedStatement ps = con.prepareStatement("select * from to_do_list where is_done="+is_done);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
            	ListaBean lis =new ListaBean(rs.getInt("id"), rs.getString("list"),rs.getBoolean("is_done"));
            	texto.add(lis);
            }
       }catch(Exception e) {
           System.out.println(e);
           return texto;
       }
       return texto;
       
   }
	
   public static List<ListaBean> getListaDone() {
       List<ListaBean> texto = new ArrayList<ListaBean>();
       Conexion co=new Conexion();
       try {
           Connection con = co.getConnection();
           PreparedStatement ps = con.prepareStatement("select * from to_do_list where is_done=1");
           ResultSet rs = ps.executeQuery();
           while(rs.next()) {
           	ListaBean lis =new ListaBean(rs.getInt("id"), rs.getString("list"),rs.getBoolean("is_done"));
           	texto.add(lis);
           }
       }catch(Exception e) {
           System.out.println(e);
           return texto;
       }
       
       return texto;
       
   }
    
}
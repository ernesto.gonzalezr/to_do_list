package com.softtek.academy.javaweb.beans;

public class ListaBean {
int id;
String texto;
Boolean done;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTexto() {
	return texto;
}
public void setTexto(String texto) {
	this.texto = texto;
}
public Boolean getDone() {
	return done;
}
public void setDone(Boolean done) {
	this.done = done;
}
public ListaBean(int id, String texto, Boolean done) {
	super();
	this.id = id;
	this.texto = texto;
	this.done = done;
}

}